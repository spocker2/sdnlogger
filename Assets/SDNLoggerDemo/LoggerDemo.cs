

using UnityEngine;
using System.Collections;
using SDN;

public class LoggerDemo : MonoBehaviour {
	
	private int i;

	// Use this for initialization
	void Start () {

		Logger.Instance.LoggerPath = "c:\\temp\\";
		Logger.Instance.LoggerName = "test";
		Logger.Instance.ShowWatch = true;

		D.Error("Test");
		D.Error("Test {0}", 1);
		D.Warn("Test");
		D.Warn("Test {0}", 1);
		D.Log("Test");
		D.Log("Test {0}", 1);
		D.Log(LogCategory.Game, "Test {0}", 2);
		D.Detail("Test");
		D.Detail("Test {0}", 1);
		D.Detail(LogCategory.Game, "Test {0}", 2);
		D.Trace("Test");
		D.Trace("Test {0}", 1);
		D.Trace(LogCategory.Game, "Test {0}", 2);
	}
	
	void Update() {
		i++;	
		D.Log("Another Log Entry");
		D.Log(LogCategory.Game, "Test i = {0}", i);
		D.Log(LogCategory.Camera, "GameObject = " + gameObject);
		D.Watch("i = ", i);
		D.Watch("time = ", System.DateTime.Now);
	}

}
