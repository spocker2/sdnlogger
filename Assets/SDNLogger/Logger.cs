
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace SDN {

	/// <summary>
	/// A Singleton class for running the logging system.
	/// </summary>
	public class Logger : MonoSingleton<Logger> {

		/// <summary>
		/// The logger path.
		/// </summary>
		private string 		loggerPath = Application.persistentDataPath + "\\";

		/// <summary>
		/// The name of the logger.
		/// </summary>
		private string 		loggerName = "dlogger";

		/// <summary>
		/// Is watching enabled?
		/// </summary>
		private bool		loggerWatch = false;

		/// <summary>
		/// Is the application currently quitting?
		/// </summary>
		private bool		isQuitting;

		/// <summary>
		/// The list of watch groups.
		/// </summary>
		private Dictionary<string, object> 	watchGroups;

		//	PUBLIC PROPERTIES

		/// <summary>
		/// Gets or sets the logger path.
		/// </summary>
		/// <value>The logger path.</value>
		public string LoggerPath {
			get { return this.loggerPath; }
			set { 
				this.loggerPath = value; 
				Debug.Log(string.Format("- logger path = {0}", loggerPath));
			}
		}

		/// <summary>
		/// Gets or sets the name of the log.
		/// </summary>
		/// <value>The name of the log.</value>
		public string LoggerName {
			get { return this.loggerName; }
			set { 
				this.loggerName = value; 
				Debug.Log(string.Format("- logger name = {0}", loggerName));
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="SDN.Logger"/> is showing Watches.
		/// </summary>
		/// <value><c>true</c> if show watch; otherwise, <c>false</c>.</value>
		public bool ShowWatch { get; set; }

		//	PUBLIC

		/// <summary>
		/// Clears all watch Groups.
		/// </summary>
		/// <param name="name">Name.</param>
		public void ClearWatch(string name) {
			if (watchGroups.ContainsKey(name)) {
				Dictionary<string, object> w = watchGroups[name] as Dictionary<string, object>;
				w.Clear();
				watchGroups[name] = w;
			}
		}

		/// <summary>
		/// Sets a Watch in the Default group
		/// </summary>
		/// <param name="key">Key.</param>
		/// <param name="val">Value.</param>
		public void SetWatch(string key, object val) {
			__setWatch("Default", key, val);
		}

		/// <summary>
		/// Sets a watch for a group
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="key">Key.</param>
		/// <param name="val">Value.</param>
		public void SetWatch(string name, string key, object val) {
			__setWatch(name, key, val);
		}
		
		//	PRIVATE

		private void __setWatch(string name, string key, object val) {
			
			// when the group name does not exist, create it
			if (!watchGroups.ContainsKey(name)) {
				Dictionary<string, object> w = new Dictionary<string, object>();
				watchGroups.Add(name, w);
			}
			
			Dictionary<string, object> _w = watchGroups[name] as Dictionary<string, object>;
			
			if (_w.ContainsKey(key)) {
				_w[key] = val;
			} else {
				_w.Add(key, val);
			}
		}
		
		private void _loadSettings() {
			//string json = SdnGames.LoadJsonResourceFile("dlogger");
			//Dictionary<string, object> _data = SdnGames.JsonDeserialize(json);	
			//Dictionary<string, object> _settings = _data["settings"] as Dictionary<string, object>;
			//_loadLoggerSettings(_settings);
		}	
		private void _loadLoggerSettings(Dictionary<string, object> settings) {
			if (settings.ContainsKey("logPath")) loggerPath = (string)settings["logPath"];
			if (settings.ContainsKey("logName"))loggerName = (string)settings["logName"];
			if (settings.ContainsKey("logWatch"))loggerWatch = (bool)settings["logWatch"];
			ShowWatch = loggerWatch;	//	set default to what is in file
		}
		
		private void __initWatch() {
			watchGroups.Clear();	
			Dictionary<string, object> _w = new Dictionary<string, object>();
			watchGroups.Add("Default", _w);
		}
		
		//	MONO

		void OnEnable() {
			Debug.Log("- DLogger started");
			watchGroups = new Dictionary<string, object>();
			_loadSettings();
			__initWatch();
		}

		void OnApplicationQuit() {
			Debug.LogWarning("- DLogger quitting");
			isQuitting = true;
		}

		void OnDestroy() {
			if (!isQuitting) return;
			D.Quit();
		}

		void OnLevelWasLoaded(int level) {
			__initWatch();
		}

		void OnGUI() {
			
			//m_loggerWatch = m_showWatches;
			
			if (!ShowWatch) return;
			
			float c = 0;
		
			float sx = 64;
			float sy = 8;
			float sw = 300;
			float sh = 20;
				
			foreach(KeyValuePair<string, object> gkvp in watchGroups) {
				
				int v = 0;		// current var #
				
				//x += 1;
				
				Dictionary<string, object> _w = watchGroups[gkvp.Key] as Dictionary<string, object>;		
					
				float _h = sh + (sh * _w.Count) + 8;
				
				if ((sy + _h) > Screen.height) { 
					sy = 8;
					c += 1;
					sx = 64 + (c * 240);
				}
				
				if (_w.Count > 0) {
					GUI.Box(new Rect(sx, sy, sw, sh + (sh * _w.Count)), gkvp.Key);
						
					foreach(KeyValuePair<string, object> kvp in _w) {
						GUI.Label(new Rect(sx, sy + sh + (sh * v), sw, sh), string.Format("{0} : {1}", kvp.Key, kvp.Value));
						v += 1;
					}
				
					sy += _h;
				}
			}
		}
	}
}